CREATE TABLE source_table (
	driverId SERIAL PRIMARY KEY, 
	driver_name VARCHAR(255),
	constructor_name VARCHAR(255)
);

INSERT INTO source_table (driver_name, constructor_name) 
VALUES 
	('lewis hamiltion', 'mercedes'),
    ('max verstappen', 'red bull '),
	('lando norris', 'mclaren' ),
	('lance stroll', 'aston martin'),
	('carlos sainz', 'ferrari');

-- Stage Table  
CREATE TABLE target_table (
	driverId INT,
	driver_name VARCHAR(255), 
	constructor_name VARCHAR(255),
    start_date TIMESTAMP ,
	end_date TIMESTAMP,
	status VARCHAR(255)
);

CREATE OR REPLACE PROCEDURE UpdateStage()
LANGUAGE plpgsql
AS $$
BEGIN
	-- Update Existing Records
	UPDATE target_table
	SET end_date = CURRENT_TIMESTAMP, status = 'in_active'
	FROM source_table
	WHERE target_table.driverId = source_table.driverId 
	AND target_table.constructor_name != source_table.constructor_name
	AND end_date = '2999-01-01';

	-- Inserting new records 
	INSERT INTO target_table (driverId, driver_name, constructor_name, start_date, end_date, status)
	SELECT src.driverId, src.driver_name, src.constructor_name, CURRENT_TIMESTAMP, '2999-01-01', 'active'
	FROM source_table src
	WHERE NOT EXISTS (
		SELECT 1 
		FROM target_table st
		WHERE st.driverId = src.driverId 
		AND st.driver_name = src.driver_name 
		AND st.constructor_name = src.constructor_name
		AND st.status = 'active'
	);
	
	-- Deleting records
	UPDATE target_table
	SET end_date = CURRENT_TIMESTAMP, STATUS = 'retired'
	WHERE NOT EXISTS (
		SELECT 1
		FROM source_table
		WHERE target_table.driverId = source_table.driverId 
		AND target_table.driver_name = source_table.driver_name
	)
	AND end_date = '2999-01-01';
END;
$$;


CALL UPDATESTAGE(); 

------------------------------------------------------------------

SELECT * FROM SOURCE_TABLE;
SELECT * FROM target_table;

UPDATE SOURCE_TABLE SET CONSTRUCTOR_NAME = 'red bull' WHERE DRIVERID = 2;
DELETE FROM SOURCE_TABLE WHERE DRIVERID = 2;

DELETE FROM SOURCE_TABLE WHERE DRIVERID = 6;

INSERT INTO SOURCE_TABLE (DRIVER_NAME, CONSTRUCTOR_NAME)
VALUES ('Schaumer', 'haas f1')

drop table source_table;
drop table target_table;

SELECT t1.driver_name, t1.constructor_name, t1.start_date, t1.end_date, t2.start_date, t2.end_date
FROM target_table AS t1
JOIN target_table AS t2 ON t1.driverid = t2.driverid
WHERE t1.start_date < t2.end_date AND t2.end_date > t1.start_date
AND t1.constructor_name != t2.constructor_name